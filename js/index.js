
/*
 * En javascript, on peut créer des objets à la volet en utilisant
 * des accolades et en spécifiant entre celle ci les différentes
 * propriétés de cet objet.
 * Un objet sera en gros une suite d'associations clef:valeur
 * document et un peu tout ce qui en découle sont des objets.
 * les Array sont des objets etc.
 */
let firstObject = {
    property1: 'valeur1',
    property2: true,
    //Un objet peut avoir un tableau comme valeur d'une propriété..
    list: ['ga', 'zo', 'bu'],
    //..ou même un autre objet..
    object: {
        prop1: 'value'
    },
    numberProp: 1,
    //..ou encore une fonction (ou méthode)
    method() {
        console.log('bloup');
    }
    // method: function() {
    //}
};
//Pour accéder aux propriétés d'un objet, on utilise le nom de
//la variable qui contient l'objet suivi d'un point et du nom de la propriété ciblée
firstObject.property1 = 'bloup';

console.log(firstObject.property1);

console.log(firstObject.list[0]);
//Et si la valeur d'une propriété est un objet également, on peut
//enchaîner les points pour accéder aux propriétés de ces sous objets
console.log(firstObject.object.prop1);
firstObject.method();