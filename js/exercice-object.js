/*
 *On crée ici un objet qui représente un stylo, en lui spécifiant
 une couleur, une taille et un type de stylo ainsi qu'une méthode
 pour écrire 
 */
let pen = {
    color: 'red',
    size: 20,
    type: 'rewritable',
    write(text) {
        let p = document.createElement('p');
        //On peut accéder aux propriétés de l'objet dans lequel
        //on se situe actuellement en utilisant le mot clef this
        //Ici, on l'utilise pour modifier la couleur et la taille
        //du paragraphe à écrire en se basant sur les valeurs de l'objet
        p.style.color = this.color;
        p.style.fontSize = this.size+'px';
        p.textContent = text;
        document.body.appendChild(p);
    }
};


console.log(pen);

pen.write('bloup');