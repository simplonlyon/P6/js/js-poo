/**
 * (pour les commentaires, voir exercice-form-user.js et user.js,
 * c'est exactement la même chose)
 */
let listDogs = [
    new Dog('spot', 'corgi', 'brown', 1),
    new Dog('fido', 'dalmachien', 'white', 2)
];

let form = document.querySelector('form');

form.addEventListener('submit', function(event){
  event.preventDefault();

  let name = document.querySelector('#name').value;
  let breed = document.querySelector('#breed').value;
  let color = document.querySelector('#color').value;
  let age = document.querySelector('#age').value;

  let dog = new Dog(name, breed, color, age);
  listDogs.push(dog);
  console.log(listDogs);


});