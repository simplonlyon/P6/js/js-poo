let form = document.querySelector('#form-user');
/**
 * On crée un tableau qui servira à "stocker" les users créés par
 * le formulaire. On le crée ici pour qu'il soit accessible de 
 * manière global et que chaque soumission du formulaire partage
 * le même tableau.
 * (le "stockage" n'est ici que temporaire, on repart à zéro à
 * chaque rechargement de page)
 */
let list = [];
/**
 * Les formulaires ont un event submit qui sera déclencher dès que
 * le formulaire en question est soumit, c'est à dire quand on click
 * sur le bouton submit ou alors que l'on tape enter à l'intérieur
 * d'un des inputs du formulaire
 */
let inputName = document.querySelector('#name');
form.addEventListener('submit', function (event) {
    /**
     * La méthode event.preventDefault() permet d'annuler le comportement
     * par défaut de l'event actuel. Ici, on s'en sert pour faire que
     * la soumission du formulaire n'entraîne pas de rechargement de page
     */
    event.preventDefault();
    //On récupère toutes les valeurs des inputs du formulaire
    //(manière alternative plus bas)

    let inputName = document.querySelector('#name').value;
    let inputPassword = document.querySelector('#password').value;
    let inputMail = document.querySelector('#mail').value;
    let inputBirthdate = document.querySelector('#birthdate').value;
    let inputAvatar = document.querySelector('#avatar').value;

    //On utilise ces valeurs pour créer une nouvelle instance de User
    let newPerson = new User(inputName, inputPassword, inputMail, inputBirthdate, inputAvatar);
    //On ajoute cette nouvelle instance dans le tableau créé au début
    list.push(newPerson);

    console.log(list);


    // let formValues = {};
    // let inputs = form.querySelectorAll('input, select, textarea');
    // for(let element of inputs) {
    //     formValues[element.id] = element.value;
    // }
    
    // console.log(formValues);

});

let btnShow = document.querySelector('#show');
btnShow.addEventListener('click', function () {
    displayUsers(list);
});
/**
 * La fonction displayUsers a pour but de prendre la list de users
 * et de généré l'affichage à partir de ces users.
 * On a donc la partie visuel de l'appli qui découle de la partie
 * données de l'appli.
 * @param {User[]} userList La liste de user à afficher dans le html
 */
function displayUsers(userList) {
    let target = document.querySelector('#user-list');
    target.innerHTML = '';
    for (let user of userList) {
        target.appendChild(user.toHTML());
    }
}