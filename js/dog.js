class Dog {
  constructor(name, breed, color, age){
    this.name = name;
    this.breed = breed;
    this.color = color;
    this.age = age;
  }
}