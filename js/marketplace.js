"use strict";

class User {
  constructor(name, surname, email, birthdate, address, gender){
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.birthdate = birthdate;
    this.address = address;
    this.gender = gender;
  }

  _checkTypeString(variable){
    return typeof(variable) === 'string' && variable !== '';
  }

  setEmail(email){
    if (this._checkTypeString(email)) {
      this.email = email;
    }
  }
  setName(name){
    this.name = name;
  }
  setSurname(surname){
    this.surname = surname;
  }
  setBirthdate(birthdate){
    this.birthdate = birthdate;
  }
  setAddress(address){
    this.address = address;
  }
  setGender(gender){
    this.gender = gender;
  }
}

let bob = new User('bob', 'moran', 'bob@gmail.com', '01/01/1970', '123 rue bidon 69001 Lyon', 1);