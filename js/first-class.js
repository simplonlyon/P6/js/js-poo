/* On crée ici des instances de la classe Pen en utilisant le mot
clef new suivi du nom de la classe à instanciée, suivi des arguments
attendus par le constructeur de la classe en question. 
 */
let pen = new Pen('red', 17);
let bigPen = new Pen('blue', 53);


pen.write('kekchose');

bigPen.write('autre');