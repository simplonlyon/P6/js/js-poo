

class User {
    constructor(paramName,paramPassword,paramMail,paramBirthdate,paramAvatar = ''){
        this.name = paramName;
        this.password = paramPassword;
        this.mail = paramMail;
        this.birthdate = paramBirthdate;
        this.avatar = paramAvatar;
    }
    /**
     * La méthode toHTML va créer un élément HTML représentant
     * le user actuel et va le return pour qu'il soit utilisé à
     * l'extérieur
     */
    toHTML() {
        let article = document.createElement('article');
        article.textContent = `${this.name} ${this.mail} ${this.birthdate} ${this.password} ${this.avatar}`;

        return article;
    }
        
    
}

// let user = new User('Simplon', 'simplon', 'simplon@simplon.co', '12/06/2013');
// user.toHTML();

/*
let pName = document.createElement('p');
        pName.textContent = this.name;
        article.appendChild(pName);
        let pPass = document.createElement('p');
        pPass.textContent = this.password;
        article.appendChild(pPass);
        let pMail = document.createElement('p');
        pMail.textContent = this.mail;
        article.appendChild(pMail);
        let pBirthdate = document.createElement('p');
        pBirthdate.textContent = this.birthdate;
        article.appendChild(pBirthdate);
        let img = document.createElement('img');
        img.src = this.avatar;
        article.appendChild(img);

*/